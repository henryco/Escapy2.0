package net.irregular.escapy.utils;

/**
 * @author Henry on 29/06/17.
 */
public interface EscapyNamed {
	String getName();
}
