package net.irregular.escapy.utils;

import com.badlogic.gdx.utils.Disposable;

/**
 * @author Henry on 13/07/17.
 */
public interface EscapyObject extends EscapyNamed, Disposable {
}
