package net.irregular.escapy.utils.container;

// TODO: Auto-generated Javadoc
/**
 * The Interface EscapyContainerable.
 */
@Deprecated
public interface EscapyContainerable {

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	void setID(int id);
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	int getID();
}
