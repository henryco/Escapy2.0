package net.irregular.escapy.group.map.core.layer.shift;

/**
 * @author Henry on 12/07/17.
 */
public interface LayerShiftLogic {

	float[] calculateShift(LayerShift shift);
}
