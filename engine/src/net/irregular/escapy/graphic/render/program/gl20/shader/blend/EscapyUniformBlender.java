package net.irregular.escapy.graphic.render.program.gl20.shader.blend;

import net.irregular.escapy.graphic.render.program.gl20.core.EscapyMultiSourceShader;
import net.irregular.escapy.graphic.render.program.gl20.core.uniform.StandardUniformsProvider;

/**
 * @author Henry on 01/07/17.
 */
public interface EscapyUniformBlender extends EscapyMultiSourceShader, StandardUniformsProvider {

}
