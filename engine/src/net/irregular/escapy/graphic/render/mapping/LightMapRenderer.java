package net.irregular.escapy.graphic.render.mapping;

import com.badlogic.gdx.graphics.g2d.Batch;

/**
 * @author Henry on 12/07/17.
 */
public interface LightMapRenderer {
	void renderLightMap(Batch batch);
}
