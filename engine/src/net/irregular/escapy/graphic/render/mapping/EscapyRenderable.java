package net.irregular.escapy.graphic.render.mapping;


/**
 * @author Henry on 25/06/17.
 */
public interface EscapyRenderable extends GraphicRenderer, NormalMapRenderer, LightMapRenderer {


}