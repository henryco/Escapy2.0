package net.irregular.escapy.graphic.render.mapping;

import com.badlogic.gdx.graphics.g2d.Batch;


/**
 * @author Henry on 28/06/17.
 */
public interface GraphicRenderer {

	void renderGraphics(Batch batch);
}
