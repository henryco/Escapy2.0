# ![Escapy2.0](https://raw.githubusercontent.com/henryco/Escapy2.0/master/promo/ESCAPY.png)
<h3>Java 2D game engine</h3>

   * <a href="https://github.com/henryco/Escapy2.0/blob/master/engine/doc/tex/Escapy2Doc.pdf">**Engine Documentation and User Guide**</a>
   * <a href="https://github.com/henryco/Escapy-des-doc">**Game Design document**</a>
   * <a href="https://github.com/henryco/Escapy">**EscapyGdx (old)**</a>
   * <a href="http://square.github.io/dagger/">**Dagger**</a>
<br>

<img src="https://raw.githubusercontent.com/henryco/Escapy/master/promo/esWeather.png" />
<img src="https://raw.githubusercontent.com/henryco/Escapy/master/promo/ims3.png" />
