package net.irregular.escapy.modules.pause;


import net.irregular.escapy.context.annotation.ScreenName;
import net.irregular.escapy.context.game.screen.EscapyScreen;
import net.irregular.escapy.context.game.screen.EscapyScreenContext;

@ScreenName("pause_screen")
public class PauseScreen implements EscapyScreen {

	@Override
	public void show() {}

	@Override
	public void render(float delta) {}

	@Override
	public void resize(int width, int height) {}

	@Override
	public void setScreenContext(EscapyScreenContext screenContext) {}

}
